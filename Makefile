.PHONY: _list

_list:
	@echo "Type make then a space then hit tab to see available commands"

build:
	docker-compose --project-name=ldl build

stop:
	docker-compose --project-name=ldl stop

rm:
	docker-compose --project-name=ldl rm -f

# make up_with_custom_config path={path-to-your-file-from-project-root}
up_with_custom_config:
	/bin/bash _devops/config/assembly.sh /app/${path} ${lesson_viewer}

up:
	docker-compose --project-name=ldl up

# run my.cs
# private_package: make install_package url=git+https://your-package-url#version
# regular_package: make install_package url=axios#1.0
install_package:
	docker exec -it mycs-nodejs bash -- /home/maintain.sh add ${url}

# run my.cs and make reinstall_npm
reinstall_npm:
	docker exec -it mycs-nodejs bash /home/maintain.sh install

# in case if you got an error, that circusstreet_circusstreet network not exists, just run it!
create_network:
	docker network create circusstreet_circusstreet