FROM php:7.4-apache

# COPY _devops/php.ini /usr/local/etc/php/

RUN apt-get update && apt-get install -y \
  git \
  zip \
  unzip \
  libssl-dev \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  libpq-dev \
  acl \
  nano \
  zlib1g-dev

#The php zip extension requires zlib wich is a data-compression library. That's why you have to install zlib1g-dev first.
# RUN docker-php-ext-install zip

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=1.10.8

# Configure Apache
# RUN a2enmod rewrite && echo "ServerName localhost" >> /etc/apache2/apache2.conf
COPY _devops/ldl.dev.conf /etc/apache2/sites-available
RUN a2ensite ldl.dev
RUN a2enmod rewrite

# Add vendor binaries to PATH
ENV PATH=/var/www/html/vendor/bin:$PATH

# Change workdir
WORKDIR /var/www/html

# Copy files
COPY . /var/www/html

# Composer empty global config to avoid warning
# RUN echo "{}" > ~/.composer/composer.json

# Composer install
# RUN COMPOSER_ALLOW_SUPERUSER=1 composer install --prefer-dist --optimize-autoloader --profile --ignore-platform-reqs -vvv --no-interaction

# HEALTHCHECK --interval=1m --timeout=1s \
#   CMD curl -f http://localhost/api/is_alive || exit 1

# COPY _devops/run.sh /run.sh
# CMD run.sh
CMD apache2-foreground
# CMD _devops/run.sh
